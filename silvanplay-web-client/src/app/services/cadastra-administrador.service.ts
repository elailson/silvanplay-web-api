import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { environmentProd } from '../../environments/environment.prod';
import { LocalStorageService } from 'angular-web-storage';
import { Administrador } from '../model/Administrador';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class CadastroAdministradorService {

    private relativePath: string = environment.url;
    // private relativePath: string = environmentProd.url;

    private token = this.storageService.get('token');

    constructor(private http: HttpClient, private storageService: LocalStorageService) { }

    cadastra(administrador: Administrador): Observable<Administrador> {
        return this.http.post<Administrador>(this.relativePath + 'administrador/save', administrador, { headers: this.getHeaders() });
    }

    buscaTodos(): Observable<Array<Administrador>> {
        return this.http.get<Array<Administrador>>(this.relativePath + 'administrador/buscaTodos', { headers: this.getHeaders() });
    }

    atualizaDados(administrador: Administrador): Observable<any> {
        return this.http.put(this.relativePath + 'administrador/update', administrador, { headers: this.getHeaders() });
    }

    deleta(id: number): Observable<any> {
        return this.http.delete(this.relativePath + 'administrador/delete/' + id, { headers: this.getHeaders() });
    }

    protected getHeaders() {
        console.log('TOKEN SERVICE ADMINISTRADOR => ' + this.token);
        const headers = new HttpHeaders();
        headers.set('Content-Type', 'application/json');
        headers.set('Authorization', this.token);
        return headers;
    }
}