package br.com.silvanplaywebapi.service;

import br.com.silvanplaywebapi.model.Roteiro;
import br.com.silvanplaywebapi.repository.RoteiroRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class RoteiroService {

    @Autowired
    private RoteiroRepository roteiroRepository;

    @PersistenceContext
    private EntityManager entityManager;

    public Roteiro save(Roteiro roteiro) {
        return roteiroRepository.save(roteiro);
    }

    public List<Roteiro> findAll() {
        return roteiroRepository.findAll();
    }

    public List<Roteiro> search(String nome, String campus, String roteiro) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Roteiro> criteriaQuery = criteriaBuilder.createQuery(Roteiro.class);
        Root<Roteiro> tipoOcorrenciaRoot = criteriaQuery.from(Roteiro.class);

        List<Predicate> predicates = new ArrayList<>();
        if (!nome.equals("null")) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.lower(tipoOcorrenciaRoot.get("nome")), nome.trim().toLowerCase() + "%"));
        }
        if (!campus.equals("null")) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.lower(tipoOcorrenciaRoot.get("campus")), campus.trim().toLowerCase() + "%"));
        }
        if (!roteiro.equals("null")) {
            predicates.add(criteriaBuilder.equal(tipoOcorrenciaRoot.get("roteiro"), roteiro));
        }
        criteriaQuery.orderBy(Collections.singletonList(criteriaBuilder.asc(tipoOcorrenciaRoot.get("nome"))));
        criteriaQuery.select(tipoOcorrenciaRoot).where(predicates.toArray(new Predicate[]{}));

        return entityManager.createQuery(criteriaQuery).getResultList();
    }
}
