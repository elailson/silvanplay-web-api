package br.com.silvanplaywebapi.controller;

import br.com.silvanplaywebapi.model.Roteiro;
import br.com.silvanplaywebapi.service.RoteiroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/roteiro")
public class RoteiroController {

    @Autowired
    private RoteiroService roteiroService;

    @PostMapping("/save")
    public ResponseEntity<Roteiro> save(@RequestBody Roteiro roteiro) {
        return ResponseEntity.created(URI.create("")).body(roteiroService.save(roteiro));
    }

    @GetMapping
    public ResponseEntity<List<Roteiro>> findAll() {
        return ResponseEntity.ok(roteiroService.findAll());
    }

    @GetMapping("/search/{nome}/{campus}/{roteiro}")
    public ResponseEntity<List<Roteiro>> search(@PathVariable String nome, @PathVariable String campus, @PathVariable String roteiro) {
        return ResponseEntity.ok(roteiroService.search(nome, campus, roteiro));
    }
}
