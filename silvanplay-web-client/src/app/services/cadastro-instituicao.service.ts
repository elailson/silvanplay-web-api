import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { environmentProd } from '../../environments/environment.prod';
import { Instituicao } from '../model/Instituicao';

@Injectable({providedIn: 'root'})
export class CadastroInstituicaoService {

    private relativePath: string = environment.url;
    // private relativePath: string = environmentProd.url;

    constructor(private http: HttpClient) { }

    cadastrar(instituicao: Instituicao) {
        return this.http.post(this.relativePath + 'educacao', instituicao, { headers: this.getHeaders() });
    }

    protected getHeaders() {
        const headers = new HttpHeaders();
        headers.set('Content-Type', 'application/json');
        return headers;
    }
}