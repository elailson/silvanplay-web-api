import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { LocalStorageService } from 'angular-web-storage';
import { AdminEnum } from 'src/app/enum/AdminEnum';

export class AuthGuardPerfil2 implements CanActivate {

    constructor(private router: Router,
        private localStorageService: LocalStorageService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        let retorno = false;
        const permissoes = this.localStorageService.get('permissoes');

        if (permissoes) {
            permissoes.forEach((permissao: string) => {
                if (permissao.trim() === AdminEnum.EDUCACAO) {
                    retorno = true;
                }
            });
            return retorno;
        }
        this.router.navigate(['home']);
        return retorno;
    }
}