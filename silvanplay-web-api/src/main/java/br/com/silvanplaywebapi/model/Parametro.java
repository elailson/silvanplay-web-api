package br.com.silvanplaywebapi.model;

import javax.persistence.*;

@Entity
@Table(name = "PARAMETRO")
public class Parametro {

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "MATRICULA_DATA_INICIAL")
    private String matriculaDataInicial;

    @Column(name = "MATRICULA_DATA_FINAL")
    private String matriculaDataFinal;

    @Column(name = "RENOVACAO_DATA_INICIAL")
    private String renovacaoDataInicial;

    @Column(name = "RENOVACAO_DATA_FINAL")
    private String renovacaoDataFinal;

    @Transient
    private Integer periodoMatricula;

    @Transient
    private Integer periodoRenovacao;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMatriculaDataInicial() {
        return matriculaDataInicial;
    }

    public void setMatriculaDataInicial(String matriculaDataInicial) {
        this.matriculaDataInicial = matriculaDataInicial;
    }

    public String getMatriculaDataFinal() {
        return matriculaDataFinal;
    }

    public void setMatriculaDataFinal(String matriculaDataFinal) {
        this.matriculaDataFinal = matriculaDataFinal;
    }

    public String getRenovacaoDataInicial() {
        return renovacaoDataInicial;
    }

    public void setRenovacaoDataInicial(String renovacaoDataInicial) {
        this.renovacaoDataInicial = renovacaoDataInicial;
    }

    public String getRenovacaoDataFinal() {
        return renovacaoDataFinal;
    }

    public void setRenovacaoDataFinal(String renovacaoDataFinal) {
        this.renovacaoDataFinal = renovacaoDataFinal;
    }

    public Integer getPeriodoMatricula() {
        return periodoMatricula;
    }

    public void setPeriodoMatricula(Integer periodoMatricula) {
        this.periodoMatricula = periodoMatricula;
    }

    public Integer getPeriodoRenovacao() {
        return periodoRenovacao;
    }

    public void setPeriodoRenovacao(Integer periodoRenovacao) {
        this.periodoRenovacao = periodoRenovacao;
    }
}
