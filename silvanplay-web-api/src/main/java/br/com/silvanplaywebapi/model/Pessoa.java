package br.com.silvanplaywebapi.model;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.sql.Timestamp;

@MappedSuperclass
public abstract class Pessoa {

    @Column(name = "NOME")
    protected String nome;

    @Column(name = "CPF")
    protected String cpf;

    @Column(name = "DATA_NASCIMENTO")
    protected String dataNascimento;

    @Column(name = "TELEFONE")
    protected String telefone;

    @Column(name = "SEXO")
    protected Integer sexo;

    @Column(name = "RACA")
    protected Integer raca;

    @Column(name = "DATA_CRIACAO")
    protected Timestamp dataCriacao;

    @Column(name = "DEFICIENCIA")
    protected Integer deficiencia;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(String dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public Integer getSexo() {
        return sexo;
    }

    public void setSexo(Integer sexo) {
        this.sexo = sexo;
    }

    public Integer getRaca() {
        return raca;
    }

    public void setRaca(Integer raca) {
        this.raca = raca;
    }

    public Timestamp getDataCriacao() {
        return dataCriacao;
    }

    public void setDataCriacao(Timestamp dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

    public Integer getDeficiencia() {
        return deficiencia;
    }

    public void setDeficiencia(Integer deficiencia) {
        this.deficiencia = deficiencia;
    }
}