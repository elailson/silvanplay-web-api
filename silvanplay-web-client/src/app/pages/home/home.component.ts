import { Component, OnInit, ViewChild } from '@angular/core';
import { BaseChartDirective } from 'ng2-charts';
import { Dataset } from 'src/app/interfaces/dataset';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public barChartOptions: any = {
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
            minTicksLimit: 5,
            maxTicksLimit: 8
          }
        }
      ]
    },
    scaleShowVerticalLines: false,
    responsive: true,
  };

  public barChartLabels: string[] = ['2020.1'];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = true;

  public barChartData: any[] = [
    { data: [147], label: 'Matutino' },
    { data: [113], label: 'Vespertino' },
    { data: [230], label: 'Noturno' }
  ];

  constructor() { }

  ngOnInit() {
  }

  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }
}