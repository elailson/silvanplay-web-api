import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadastraAdministradorComponent } from './cadastra-administrador.component';

describe('CadastraAdministradorComponent', () => {
  let component: CadastraAdministradorComponent;
  let fixture: ComponentFixture<CadastraAdministradorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadastraAdministradorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastraAdministradorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
