package br.com.silvanplaywebapi.service;

import br.com.silvanplaywebapi.model.Solicitacao;
import br.com.silvanplaywebapi.repository.SolicitacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SolicitacaoService {

    @Autowired
    private SolicitacaoRepository solicitacaoRepository;

    public List<Solicitacao> findAll() {
        return solicitacaoRepository.findAll();
    }
}
