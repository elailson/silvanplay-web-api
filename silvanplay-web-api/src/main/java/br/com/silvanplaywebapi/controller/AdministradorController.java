package br.com.silvanplaywebapi.controller;

import br.com.silvanplaywebapi.config.security.TokenService;
import br.com.silvanplaywebapi.model.Administrador;
import br.com.silvanplaywebapi.repository.AdministradorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/administrador")
public class AdministradorController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private AdministradorRepository administradorRepository;

    @PostMapping("/login")
    @ResponseBody
    public ResponseEntity<Administrador> autentica(@RequestBody Administrador admLogin) {
        UsernamePasswordAuthenticationToken login = admLogin.converter();
        try {
            Authentication authentication = authenticationManager.authenticate(login);
            String token = tokenService.geraToken(authentication);
            Administrador administrador = administradorRepository.findByEmail(admLogin.getUsuario());
            administrador.setToken("Bearer " + token);
            return ResponseEntity.ok(administrador);
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @PostMapping("/save")
    @ResponseBody
    public ResponseEntity<Administrador> criaLogin(@RequestBody Administrador administrador) {
        try {
            administrador.setSenha(new BCryptPasswordEncoder().encode(administrador.getSenha()));
            return ResponseEntity.ok(administradorRepository.save(administrador));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @PutMapping("/update")
    @ResponseBody
    public void atualizaDados(@RequestBody Administrador adm) {
        try {
            adm.setSenha(new BCryptPasswordEncoder().encode(adm.getSenha()));
            administradorRepository.update(adm.getId(), adm.getUsuario(), adm.getSenha(), adm.getPermissao());
        } catch (Exception ex) {
            ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @DeleteMapping("/delete/{id}")
    @ResponseBody
    public void delete(@PathVariable Long id) {
        try {
            administradorRepository.deleteById(id);
        } catch (Exception ex) {
            ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @GetMapping("/buscaTodos")
    @ResponseBody
    public ResponseEntity<Iterable<Administrador>> buscaTodos() {
        try {
            return ResponseEntity.ok(administradorRepository.findAll());
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }
}
