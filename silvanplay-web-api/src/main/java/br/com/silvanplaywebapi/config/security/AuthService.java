package br.com.silvanplaywebapi.config.security;

import br.com.silvanplaywebapi.model.Administrador;
import br.com.silvanplaywebapi.repository.AdministradorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class AuthService implements UserDetailsService {

    @Autowired
    private AdministradorRepository administradorRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Administrador administrador = administradorRepository.findByEmail(email);
        if (administrador != null) {
            return administrador;
        }
        throw new UsernameNotFoundException("Dados inválidos");
    }
}
