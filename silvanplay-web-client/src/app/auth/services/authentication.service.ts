import { Injectable } from '@angular/core';
import { Administrador } from 'src/app/model/Administrador';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ToasterService } from 'angular2-toaster';
import { Router } from '@angular/router';
import { LocalStorageService } from 'angular-web-storage';
import { environment } from 'src/environments/environment';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {

    private relativePath: string = environment.url;
    // private relativePath: string = environmentProd.url;

    constructor(private http: HttpClient) { }

    autentica(login: Administrador): Observable<Administrador> {
        return this.http.post<Administrador>(this.relativePath + 'administrador/login', login, { headers: this.getHeaders() });
    }

    protected getHeaders() {
        const headers = new HttpHeaders();
        headers.set('Content-Type', 'application/json');
        return headers;
    }
}