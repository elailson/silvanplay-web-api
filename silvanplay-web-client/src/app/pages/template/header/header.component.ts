import { Component, OnInit, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorageService } from 'angular-web-storage';
import { AdminEnum } from 'src/app/enum/AdminEnum';

@Component({
  selector: 'header-component',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

@Injectable()
export class HeaderComponent implements OnInit {

  usuario: string;

  perfil2: boolean = false;
  perfil5: boolean = false;
  perfil7: boolean = false;
  perfil10: boolean = false;

  constructor(private router: Router,
    private localStorage: LocalStorageService) { }

  ngOnInit() {
    this.usuario = this.localStorage.get('usuario');
    let permissoes = this.localStorage.get('permissoes');
    if (permissoes) {
      this.setaPerfis(permissoes);
    }
  }

  telaCadastro(tela: number) {
    switch (tela) {
      case 1:
        this.router.navigate(['cadastra-instituicao']);
        break;
      case 2:
        this.router.navigate(['cadastra-telefone']);
        break;
      case 3:
        this.router.navigate(['gerencia-solicitacoes']);
        break;
      case 4:
        this.router.navigate(['cadastra-administrador']);
        break;
    }
  }

  setaPerfis(permissoes: Array<string>) {
    permissoes.forEach(permissao => {
      switch (permissao.trim()) {
        case AdminEnum.EDUCACAO:
          this.perfil2 = true;
          break;
        case AdminEnum.GERENCIA_SOLICITACAO:
          this.perfil5 = true;
          break;
        case AdminEnum.ADM_MASTER:
          this.perfil7 = true;
          break;
        case AdminEnum.TELEFONE:
          this.perfil10 = true;
          break;
      }
    });
  }

  sair() {
    this.localStorage.clear();
  }
}
