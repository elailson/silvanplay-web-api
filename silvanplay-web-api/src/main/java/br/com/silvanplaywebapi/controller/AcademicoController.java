package br.com.silvanplaywebapi.controller;

import br.com.silvanplaywebapi.model.Academico;
import br.com.silvanplaywebapi.repository.AcademicoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/academico")
public class AcademicoController {

    @Autowired
    private AcademicoRepository academicoRepository;

    @GetMapping("/{id}")
    public ResponseEntity<Academico> findById(@PathVariable Long id) {
        Optional<Academico> academico = academicoRepository.findById(id);
        if (academico.isPresent()) {
            return ResponseEntity.ok(academico.get());
        }
        throw new RuntimeException("Acadêmico não identificado!");
    }

    @GetMapping
    public ResponseEntity<List<Academico>> findAll() {
        return ResponseEntity.ok(academicoRepository.findAll());
    }
}
