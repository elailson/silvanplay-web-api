package br.com.silvanplaywebapi.model;

public class Home {
    private Integer totalUsuario;
    private Integer totalAcademico;

    public Integer getTotalUsuario() {
        return totalUsuario;
    }

    public void setTotalUsuario(Integer totalUsuario) {
        this.totalUsuario = totalUsuario;
    }

    public Integer getTotalAcademico() {
        return totalAcademico;
    }

    public void setTotalAcademico(Integer totalAcademico) {
        this.totalAcademico = totalAcademico;
    }
}
