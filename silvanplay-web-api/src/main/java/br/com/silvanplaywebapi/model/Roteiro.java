package br.com.silvanplaywebapi.model;

import javax.persistence.*;

@Entity
@Table(name = "ROTEIRO")
public class Roteiro {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "NOME")
    private String nome;

    @Column(name = "CAMPUS")
    private String campus;

    @Column(name = "ROTEIRO")
    private Integer roteiro;

    @Column(name = "TURNO")
    private Integer turno;

    @Column(name = "TRAJETO")
    private String trajeto;

    @Column(name = "HORARIO")
    private String horario;

    @Column(name = "ASSENTOS_DISPONIVEIS")
    private Integer assentosDisponiveis;

    @Column(name = "HORA_SAIDA_MANHA")
    private String horaSaidaManha;

    @Column(name = "HORA_VOLTA_MANHA")
    private String horaVoltaManha;

    @Column(name = "HORA_SAIDA_TARDE")
    private String horaSaidaTarde;

    @Column(name = "HORA_VOLTA_TARDE")
    private String horaVoltaTarde;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCampus() {
        return campus;
    }

    public void setCampus(String campus) {
        this.campus = campus;
    }

    public Integer getRoteiro() {
        return roteiro;
    }

    public void setRoteiro(Integer roteiro) {
        this.roteiro = roteiro;
    }

    public Integer getTurno() {
        return turno;
    }

    public void setTurno(Integer turno) {
        this.turno = turno;
    }

    public String getTrajeto() {
        return trajeto;
    }

    public void setTrajeto(String trajeto) {
        this.trajeto = trajeto;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public Integer getAssentosDisponiveis() {
        return assentosDisponiveis;
    }

    public void setAssentosDisponiveis(Integer assentosDisponiveis) {
        this.assentosDisponiveis = assentosDisponiveis;
    }

    public String getHoraSaidaManha() {
        return horaSaidaManha;
    }

    public void setHoraSaidaManha(String horaSaidaManha) {
        this.horaSaidaManha = horaSaidaManha;
    }

    public String getHoraVoltaManha() {
        return horaVoltaManha;
    }

    public void setHoraVoltaManha(String horaVoltaManha) {
        this.horaVoltaManha = horaVoltaManha;
    }

    public String getHoraSaidaTarde() {
        return horaSaidaTarde;
    }

    public void setHoraSaidaTarde(String horaSaidaTarde) {
        this.horaSaidaTarde = horaSaidaTarde;
    }

    public String getHoraVoltaTarde() {
        return horaVoltaTarde;
    }

    public void setHoraVoltaTarde(String horaVoltaTarde) {
        this.horaVoltaTarde = horaVoltaTarde;
    }
}
