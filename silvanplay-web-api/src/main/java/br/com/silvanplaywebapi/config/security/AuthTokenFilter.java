package br.com.silvanplaywebapi.config.security;

import br.com.silvanplaywebapi.model.Administrador;
import br.com.silvanplaywebapi.repository.AdministradorRepository;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class AuthTokenFilter extends OncePerRequestFilter {

    private final TokenService tokenService;
    private final AdministradorRepository administradorRepository;

    private String ultimoIp;
    private LocalTime time;
    private int requisicoes = 0;
    private List<String> listaNegra = new ArrayList<>();

    public AuthTokenFilter(TokenService tokenService, AdministradorRepository administradorRepository) {
        this.tokenService = tokenService;
        this.administradorRepository = administradorRepository;
        time = LocalTime.now();
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        if (evitaAtaque(request.getRemoteAddr())) {
            String token = recuperaToken(request);
            boolean valido = tokenService.isTokenValido(token);
            if (valido) {
                autenticaAdministrador(token);
            }
            filterChain.doFilter(request, response);
        } else {
            throw new ServletException("Seu IP foi bloqueado");
        }
    }

    private String recuperaToken(HttpServletRequest request) {
        String token = request.getHeader("Authorization");
        if (token == null || token.isEmpty() || !token.startsWith("Bearer ")) {
            return null;
        }
        return token.substring(7);
    }

    private void autenticaAdministrador(String token) {
        Long idAdmin = tokenService.getIdAdministrador(token);
        Optional<Administrador> usuario = administradorRepository.findById(idAdmin);
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(usuario, null, null);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    private boolean evitaAtaque(String ip) {
        if (listaNegra.contains(ip)) return false;
        LocalTime now = LocalTime.now();
        if (ip.equals(ultimoIp) && Duration.between(time, now).getSeconds() < 5) {
            if (requisicoes > 5) {
                listaNegra.add(ip);
                return false;
            }
            requisicoes++;
            return true;
        }
        requisicoes = 0;
        time = LocalTime.now();
        ultimoIp = ip;
        return true;
    }
}
