package br.com.silvanplaywebapi.repository;

import br.com.silvanplaywebapi.model.Academico;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AcademicoRepository extends JpaRepository<Academico, Long> {
}
