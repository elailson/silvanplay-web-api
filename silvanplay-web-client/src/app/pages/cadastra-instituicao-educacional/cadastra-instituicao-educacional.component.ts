import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { CadastroInstituicaoService } from 'src/app/services/cadastro-instituicao.service';
import { ToasterService } from 'angular2-toaster';
import { Instituicao } from 'src/app/model/Instituicao';
import { LocalStorageService } from 'angular-web-storage';
import { Router } from '@angular/router';
import { Administrador } from 'src/app/model/Administrador';
import { AdminEnum } from 'src/app/enum/AdminEnum';

@Component({
  selector: 'app-cadastra-instituicao-educacional',
  templateUrl: './cadastra-instituicao-educacional.component.html',
  styleUrls: ['./cadastra-instituicao-educacional.component.css']
})
export class CadastraInstituicaoEducacionalComponent implements OnInit {

  @BlockUI() blockUI: NgBlockUI;
  instituicao: FormGroup;
  objInstituicao: Instituicao;

  segmentos = [
    { viewValue: 'Educação Infantil (Creche)', value: 0 },
    { viewValue: 'Ensino Fundamental 1', value: 1 },
    { viewValue: 'Ensino Fundamental 2', value: 2 },
    { viewValue: 'Ensino Médio', value: 3 },
    { viewValue: 'Ensino Técnico', value: 4 },
    { viewValue: 'Ensino Superior', value: 5 },
    { viewValue: 'E.J.A', value: 6 }
  ];

  turnos = [
    { viewValue: 'Matutino', value: 0 },
    { viewValue: 'Vespertino', value: 1 },
    { viewValue: 'Noturno', value: 2 }
  ];

  integrais = [
    { viewValue: 'Sim', value: 1 },
    { viewValue: 'Não', value: 0 },
  ];

  funcionais = [
    { viewValue: 'Sim', value: 1 },
    { viewValue: 'Não', value: 0 }
  ];

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private cadastraInstituicaoService: CadastroInstituicaoService,
    private toasterService: ToasterService,
    private localStorage: LocalStorageService) { }

  ngOnInit() {
    // this.validaUsuarioLogado();
    this.instituicao = this.formBuilder.group({
      nome: ['', Validators.required],
      bairro: ['', Validators.required],
      rua: ['', Validators.required],
      numero: [''],
      telefone: ['', Validators.required],
      diretor: ['', Validators.required],
      segmento: [''],
      turno: ['', Validators.required],
      integral: ['', Validators.required],
      multiFuncional: ['']
    });
  }

  cadastrar() {
    // console.log('SEGMENTOS => ' + this.instituicao.get('segmento').value);
    // console.log('TURNOS => ' + this.instituicao.get('turno').value);
    // console.log('INTEGRAL => ' + this.instituicao.get('integral').value);
    // console.log('FUNCIONAL => ' + this.instituicao.get('multiFuncional').value);

    if (this.validarFormulario()) {
      this.blockUI.start('Cadastrando');
      this.cadastraInstituicaoService.cadastrar(this.objInstituicao)
        .subscribe(retorno => {
          this.toasterService.pop('success', 'Cadastrado com sucesso!');
          this.instituicao.reset();
          this.blockUI.stop();
        }, error => {
          this.toasterService.pop('error', 'Erro ao cadastrar: ' + error);
          this.blockUI.stop();
        });
    } else {
      this.toasterService.pop('warning', 'Preencha os campos obrigatórios');
    }
  }

  validarFormulario() {
    if (this.instituicao.get('nome').value !== '' && this.instituicao.get('bairro').value !== ''
      && this.instituicao.get('rua').value !== '' && this.instituicao.get('telefone').value !== ''
      && this.instituicao.get('diretor').value !== '' && this.instituicao.get('turno').value !== '') {
      this.objInstituicao = new Instituicao(this.instituicao);
      return true;
    }
    return false;
  }

  validaUsuarioLogado() {
    let usuario = <Administrador>this.localStorage.get('usuarioLogado');
    if (usuario === null || usuario === undefined) {
      this.router.navigate(['login']);
    } else if (usuario.permissao) {
      usuario.permissao.split(',').forEach(permissao => {
        if (permissao != AdminEnum.EDUCACAO) {
          this.router.navigate(['login']);
        }
      });
    }
  }
}
