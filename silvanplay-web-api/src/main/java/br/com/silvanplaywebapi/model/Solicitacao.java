package br.com.silvanplaywebapi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SOLICITACAO")
public class Solicitacao {

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "CIDADE")
    private String cidade;

    @Column(name = "RUA")
    private String rua;

    @Column(name = "BAIRRO")
    private String bairro;

    @Column(name = "COMPLEMENTO")
    private String complemento;

    @Column(name = "LATITUDE")
    private double latitude;

    @Column(name = "LONGITUDE")
    private double longitude;

    @Column(name = "CAMINHO_IMAGEM")
    private String caminhoImagem;

    @Column(name = "CATEGORIA")
    private Integer categoria;

    @Column(name = "DESCRICAO")
    private String descricao;

    @Column(name = "STATUS")
    private Integer status;

    @Column(name = "NOTIFICADO")
    private Integer notificado;

    @Column(name = "RETORNO_USUARIO")
    private Integer retornoUsuario;

    @Column(name = "USUARIO_ID")
    private Long usuarioId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getRua() {
        return rua;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getCaminhoImagem() {
        return caminhoImagem;
    }

    public void setCaminhoImagem(String caminhoImagem) {
        this.caminhoImagem = caminhoImagem;
    }

    public Integer getCategoria() {
        return categoria;
    }

    public void setCategoria(Integer categoria) {
        this.categoria = categoria;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getNotificado() {
        return notificado;
    }

    public void setNotificado(Integer notificado) {
        this.notificado = notificado;
    }

    public Integer getRetornoUsuario() {
        return retornoUsuario;
    }

    public void setRetornoUsuario(Integer retornoUsuario) {
        this.retornoUsuario = retornoUsuario;
    }

    public Long getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(Long usuarioId) {
        this.usuarioId = usuarioId;
    }
}
