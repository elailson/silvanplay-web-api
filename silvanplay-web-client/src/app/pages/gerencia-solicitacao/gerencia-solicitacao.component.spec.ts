import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GerenciaSolicitacaoComponent } from './gerencia-solicitacao.component';

describe('GerenciaSolicitacaoComponent', () => {
  let component: GerenciaSolicitacaoComponent;
  let fixture: ComponentFixture<GerenciaSolicitacaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GerenciaSolicitacaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GerenciaSolicitacaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
