import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadastraTelefoneComponent } from './cadastra-telefone.component';

describe('CadastraTelefoneComponent', () => {
  let component: CadastraTelefoneComponent;
  let fixture: ComponentFixture<CadastraTelefoneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadastraTelefoneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastraTelefoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
