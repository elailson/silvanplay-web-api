package br.com.silvanplaywebapi.service;

import br.com.silvanplaywebapi.model.Home;
import br.com.silvanplaywebapi.repository.AdministradorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HomeService {

    @Autowired
    private AdministradorRepository administradorRepository;

    public Home buscaDados() {
        Home home = new Home();
        List<Object[]> retorno = administradorRepository.buscaDados();
        retorno.forEach(item -> {
            home.setTotalUsuario(Integer.parseInt(item[0].toString()));
            home.setTotalAcademico(Integer.parseInt(item[1].toString()));
        });

        return home;
    }
}
