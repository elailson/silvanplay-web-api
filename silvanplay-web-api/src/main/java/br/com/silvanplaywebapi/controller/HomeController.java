package br.com.silvanplaywebapi.controller;

import br.com.silvanplaywebapi.model.Home;
import br.com.silvanplaywebapi.service.HomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/home")
public class HomeController {

    @Autowired
    private HomeService homeService;

    @GetMapping("/dados")
    @ResponseBody
    public ResponseEntity<Home> buscarDados() {
        try {
            return ResponseEntity.ok(homeService.buscaDados());
        } catch (Exception ex) {
            return ResponseEntity.badRequest().build();
        }
    }
}
