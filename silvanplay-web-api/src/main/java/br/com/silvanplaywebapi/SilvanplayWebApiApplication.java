package br.com.silvanplaywebapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SilvanplayWebApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(SilvanplayWebApiApplication.class, args);
    }

}