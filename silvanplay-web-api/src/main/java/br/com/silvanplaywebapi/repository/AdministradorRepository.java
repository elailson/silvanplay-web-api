package br.com.silvanplaywebapi.repository;

import br.com.silvanplaywebapi.model.Administrador;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface AdministradorRepository extends CrudRepository<Administrador, Long> {

    @Query("SELECT administrador " +
            "FROM Administrador administrador " +
            "WHERE administrador.usuario = :usuario " +
            "AND administrador.senha = :senha")
    Administrador findByUsuarioAndSenha(String usuario, String senha);

    @Transactional
    @Modifying
    @Query(value = "UPDATE Administrador administrador " +
            "SET administrador.usuario = :usuario " +
            ", administrador.senha = :senha " +
            ", administrador.permissao = :permissoes " +
            "WHERE administrador.id = :id ")
    void update(Long id, String usuario, String senha, String permissoes);

    @Query("SELECT administrador " +
            "FROM Administrador administrador " +
            "WHERE administrador.usuario = :email")
    Administrador findByEmail(String email);

    @Query(value = "SELECT ( SELECT COUNT(*) FROM USUARIO ) AS count1, ( SELECT COUNT(*) FROM ACADEMICO ) AS count2 FROM dual", nativeQuery = true)
    List<Object[]> buscaDados();
}
